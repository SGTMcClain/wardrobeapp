//import static junit.framework.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

//import java.util.ArrayList;

//import org.junit.Test;
import org.junit.jupiter.api.Test;

import src.Shirts;

class ShirtsTest {

	@Test
	void textDescriptionTest() {
		
		Shirts shirt = new Shirts("Redskins Polo", "Nike", "Purple", "Cotton", "Polo", "XXL");
		
		String description = shirt.textDescription();
		System.out.println(description);
		assertEquals("Shirt Name: Redskins Polo, Brand: Nike, Primary Color: Purple, Style: Polo, Size: XXL", description);
		
	}

}
