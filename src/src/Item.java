/*
 * 
 */
package src;

public abstract class Item {
	private Integer itemId; //unique id provided by database
	private String name; //provided by user
	private String description; // provided by user
	private Double purchasePrice; // provided by user
	private String brand; // dropdown that could also be provided by user 
	private String primaryColor; // dropdown that could also br provided by user

	
	
	/**
	 * Instantiates a new empty item.
	 */
	public Item() {
		
	}
	
	/**
	 * Instantiates a new item with provided name, brand and primary color
	 * 
	 * @param name
	 * @param brand
	 * @param primaryColor
	 */
	public Item(String name, String brand, String primaryColor) {
		this.name = name;
		this.brand = brand;
		this.primaryColor = primaryColor;
		
	}
	
	//Item methods
	
	public void printDescription() {
		System.out.println(String.format("Item Name: %s, Brand: %s, Primary Color: %s", this.name, this.brand, this.primaryColor));
	}
	
	
	//Getters and Setters
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the purchasePrice
	 */
	public Double getPurchasePrice() {
		return purchasePrice;
	}
	/**
	 * @param purchasePrice the purchasePrice to set
	 */
	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}
	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}
	/**
	 * @return the primaryColor
	 */
	public String getPrimaryColor() {
		return primaryColor;
	}
	/**
	 * @param primaryColor the primaryColor to set
	 */
	public void setPrimaryColor(String primaryColor) {
		this.primaryColor = primaryColor;
	}
	/**
	 * @return the itemId
	 */
	public Integer getItemId() {
		return itemId;
	}
	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	
	
}
