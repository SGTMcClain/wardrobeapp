package src;

public abstract class Shoes extends Item{
	
	private String size;
	
	public Shoes() {
		
	}

	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	
	

}
