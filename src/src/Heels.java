package src;

public class Heels extends Shoes{
	
	private String heelSize;
	private String heelStyle;
	
	public Heels() {
		
	}

	/**
	 * @return the heelSize
	 */
	public String getHeelSize() {
		return heelSize;
	}

	/**
	 * @param heelSize the heelSize to set
	 */
	public void setHeelSize(String heelSize) {
		this.heelSize = heelSize;
	}

	/**
	 * @return the heelStyle
	 */
	public String getHeelStyle() {
		return heelStyle;
	}

	/**
	 * @param heelStyle the heelStyle to set
	 */
	public void setHeelStyle(String heelStyle) {
		this.heelStyle = heelStyle;
	}

}
