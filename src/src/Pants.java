package src;

public class Pants extends Clothes{
	private String pantsLength; //insteam
	private String pantsWaist;
	private String style;
	
	public Pants() {
		
	}
	
	public Pants(String name, String brand, String primaryColor, String material, String style, String length, String waist) {
		this.setName(name);
		this.setBrand(brand);
		this.setPrimaryColor(primaryColor);
		this.setMaterial(material);
		this.style = style;
		this.pantsLength = length;
		this.pantsWaist = waist;
		
	}

	/**
	 * @return the pantsLength
	 */
	public String getPantsLength() {
		return pantsLength;
	}

	/**
	 * @param pantsLength the pantsLength to set
	 */
	public void setPantsLength(String pantsLength) {
		this.pantsLength = pantsLength;
	}

	/**
	 * @return the pantsWaist
	 */
	public String getPantsWaist() {
		return pantsWaist;
	}

	/**
	 * @param pantsWaist the pantsWaist to set
	 */
	public void setPantsWaist(String pantsWaist) {
		this.pantsWaist = pantsWaist;
	}

	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}

}
