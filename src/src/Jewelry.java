package src;

public class Jewelry extends Accessories{
	private String style;
	private String metal;
	
	public Jewelry() {
		
	}

	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}

	/**
	 * @return the metal
	 */
	public String getMetal() {
		return metal;
	}

	/**
	 * @param metal the metal to set
	 */
	public void setMetal(String metal) {
		this.metal = metal;
	}

}
