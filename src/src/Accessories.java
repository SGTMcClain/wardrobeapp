package src;

public abstract class Accessories extends Item {
	
	private String materials;
	
	public Accessories() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the materials
	 */
	public String getMaterials() {
		return materials;
	}

	/**
	 * @param materials the materials to set
	 */
	public void setMaterials(String materials) {
		this.materials = materials;
	}

}
