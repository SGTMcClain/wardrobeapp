package src;

public class Shirts extends Clothes{
	private String style;
	private String size;
	
	public Shirts() {
		
	}
	
	//Constructor to handle values entered as CSV
	public Shirts(String commaSeparatedValues) {
		int i = 0;
		for (String value : commaSeparatedValues.split(",")) {
			switch(i) {
				case 0: 	super.setName(value);
							break;	
				case 1: 	super.setBrand(value);
							break;
				case 2: 	super.setPrimaryColor(value);
							break;
				case 3: 	super.setMaterial(value);
							break;
				case 4: 	this.style = value;
							break;
				case 5: 	this.size = value;
							break;
			}
			
			i++;
		}
		
	}
	
	
	public Shirts(String name, String brand, String primaryColor, String material, String style, String size) {
		super(name, brand, primaryColor, material);
		
		this.style = style;
		this.size = size;
	}
	
	//methods
	@Override
	public void printDescription() {
		
		System.out.println(String.format("Shirt Name: %s, Brand: %s, Primary Color: %s, "
										+"Style: %s, Size: %s", 
										this.getName(), this.getBrand(), this.getPrimaryColor(), this.style, this.size));
	}
	
	public String textDescription() {
		String textToPrint = String.format("Shirt Name: %s, Brand: %s, Primary Color: %s, "
				+"Style: %s, Size: %s", 
				this.getName(), this.getBrand(), this.getPrimaryColor(), this.style, this.size);
		
		return textToPrint;
	}
	
	public void printShirt() {
		System.out.println(String.format("%s is a %s %s made by %s. The size is %s.", this.getName(), this.getPrimaryColor(), this.style, this.getBrand(), this.size));
	}
	
	//Getters and Setters

	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}
	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}
}
