package src;

public abstract class Clothes extends Item{
	private String material;

	
	public Clothes() {
		
	}
	
	public Clothes(String name, String brand, String primaryColor, String material) {
		super(name, brand, primaryColor);
		this.setMaterial(material);
		
	}

	/**
	 * @return the material
	 */
	public String getMaterial() {
		return material;
	}

	/**
	 * @param material the material to set
	 */
	public void setMaterial(String material) {
		this.material = material;
	}
}
