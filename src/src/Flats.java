package src;

public class Flats extends Shoes{
	
	private String flatStyle;
	private boolean hasLaces;
	private boolean isSlipOn;
	
	public Flats() {
		
	}

	/**
	 * @return the flatStyle
	 */
	public String getFlatStyle() {
		return flatStyle;
	}

	/**
	 * @param flatStyle the flatStyle to set
	 */
	public void setFlatStyle(String flatStyle) {
		this.flatStyle = flatStyle;
	}

	/**
	 * @return the hasLaces
	 */
	public boolean getHasLaces() {
		return hasLaces;
	}

	/**
	 * @param hasLaces the hasLaces to set
	 */
	public void setHasLaces(boolean hasLaces) {
		this.hasLaces = hasLaces;
	}

	/**
	 * @return the isSlipOn
	 */
	public boolean isSlipOn() {
		return isSlipOn;
	}

	/**
	 * @param isSlipOn the isSlipOn to set
	 */
	public void setSlipOn(boolean isSlipOn) {
		this.isSlipOn = isSlipOn;
	}

}
