package src;

public class Dresses {
	private String dressSize;
	private String style;
	public Dresses() {
		
	}
	/**
	 * @return the dressSize
	 */
	public String getDressSize() {
		return dressSize;
	}
	/**
	 * @param dressSize the dressSize to set
	 */
	public void setDressSize(String dressSize) {
		this.dressSize = dressSize;
	}
	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}
	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}

}
