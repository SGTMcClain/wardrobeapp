package src;

public class Skirts extends Clothes{
	private String skirtSize;
	private String style;
	
	public Skirts() {
		
	}

	/**
	 * @return the skirtSize
	 */
	public String getSkirtSize() {
		return skirtSize;
	}

	/**
	 * @param skirtSize the skirtSize to set
	 */
	public void setSkirtSize(String skirtSize) {
		this.skirtSize = skirtSize;
	}

	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}

}
