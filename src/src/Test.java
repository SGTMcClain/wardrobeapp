/**
 * This Class implements methods necessary for homework weekly assignments 
 */

package src;


import java.io.*;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.TimeZone;

public class Test {

	public static void main(String[] args) throws IOException{
		final String TEXT_FILE = "../WardrobeApp/input.txt";
		
		
		ArrayList<String> linesOfText = getText(TEXT_FILE);

		addToWardrobe(linesOfText);

	}
	
	public static ArrayList<String> getText(String filePath) {
		ArrayList<String> linesOfText = new ArrayList<String>();
		Scanner fileScanner = null;
		
		try{
			fileScanner = new Scanner(new BufferedReader(new FileReader(filePath)));
			while(fileScanner.hasNextLine()) {
				linesOfText.add(fileScanner.nextLine());
			} 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		
		return linesOfText;
		
	}
	
	public static void addToWardrobe(ArrayList<String> linesOfText) {
		Calendar date = Calendar.getInstance();  //create a calendar object
		TimeZone timeZone = date.getTimeZone(); //get the current time zone
		date.setTimeZone(timeZone);				//set the timezone
		StringBuffer textToSave = new StringBuffer();
		final String OUTPUT_FILE = "../WardrobeApp/output.txt";
		
		for(String line : linesOfText) {
			Shirts entry = new Shirts(line);
			textToSave.append("New Item added at " + date.getTime());
			textToSave.append(System.getProperty("line.separator"));
			textToSave.append(entry.textDescription());
			textToSave.append(System.getProperty("line.separator"));
			textToSave.append(System.getProperty("line.separator"));
			
		}
		
		System.out.println(textToSave.toString());
		try {
			saveToFileSystem(OUTPUT_FILE, textToSave);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void saveToFileSystem(String saveFilePath, StringBuffer textToSave) throws IOException{
		File file = new File(saveFilePath);
		BufferedWriter save = new BufferedWriter(new FileWriter(file));
		save.write(textToSave.toString());
		save.close();
	}

}
