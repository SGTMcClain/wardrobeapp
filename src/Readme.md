5/13/2018

The test.java file runs a basic script to build an Clothes object and then output a few lines of text to ensure that it works properly.

The entire program at this point should mostly reflect the UML and uses abstract classes, inheritance, and an interface though I am not sure if I am correctly using the interface at this point.  I hope to move Test.java out of the src files and eventually use it as a proper unit test.

My full sourcecode can also be found as a private repository on GitHub at 